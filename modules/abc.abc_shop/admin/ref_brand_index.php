<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/abc.abc_shop/prolog.php");
IncludeModuleLangFile(__FILE__);

if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

CModule::IncludeModule("abc.abc_shop");

$file_module_prefix = ABCShop\UIAdmin::getFilePrefix();

$sTableID = "tbl_abc_brand_list";

$lAdmin = new CAdminList($sTableID);

$lAdmin->AddHeaders(array(
    array("id" => "id",   "content" => "Id",                 "default" => true),
    array("id" => "name", "content" => "Название",           "default" => true),
    array("id" => "sort", "content" => "Порядок сортировки", "default" => true),
));

$brandMapper = new ABCShop\Reference\BrandMapper($DB);

$res = new CDBResult;
$arNavParams = $res->GetNavParams();

$count_records = $brandMapper->count();

if (isset($arNavParams['SHOW_ALL']) && $arNavParams['SHOW_ALL']) {
    $arNav = false;
    $count_per_page = $count_records;
    $current_page = 1;
} else {
    $count_per_page = CAdminResult::GetNavSize($sTableID);

    $current_page = isset($arNavParams['PAGEN']) ? intval($arNavParams['PAGEN']) : 1;
    $offset = ($current_page - 1) * $count_per_page;

    $arNav = array('offset' => $offset, 'count' => $count_per_page);
}

$arBrand = $brandMapper->findByFilter(array('sort' => 'asc'), false, $arNav);

$res->InitFromArray($arBrand);
$res = new CAdminResult($res, $sTableID);
$res->NavStart();
$res->NavRecordCount = $count_records;
$res->NavPageCount = ceil($count_records/$count_per_page);
$res->NavPageNomer = $current_page;

$lAdmin->NavText($res->GetNavPrint('Бренды'));

foreach($arBrand as $brand) {
    $row =& $lAdmin->AddRow($brand->getId(), array(
        'id'   => $brand->getId(),
        'name' => $brand->getName(),
        'sort' => $brand->getSort(),
    ));
}

$lAdmin->CheckListMode();

$APPLICATION->SetTitle("Справочник брендов");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayList();

require($DOCUMENT_ROOT."/bitrix/modules/main/include/epilog_admin.php");
