<?php
namespace ABC\Index;

use ABCShop\ElasticSearchClient;

/**
 * ElasticSearch Индексатор новостей
 *
 */
class News
{
    const IBLOCK_TYPE = 'news';
    const IBLOCK_CODE = 'news';

    /**
     * @var boolean
     */
    private $debug;

    /**
     * @var string
     */
    private $last_error;

    /**
     * @var int
     */
    private $iblock_id;

    /**
     * @var [type]
     */
    private $es_client;

    /**
     * @param int $iblock_id
     */
    public function __construct($iblock_id)
    {
        $this->debug = false;

        $this->iblock_id = $iblock_id;

        $this->es_client = ElasticSearchClient::getInstance();
    }

    /**
     * @param  boolean $mode
     */
    public function debug($mode)
    {
        $this->debug = $mode;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return $this->last_error;
    }

    /**
     * @return boolean
     */
    public function index()
    {
        if ($this->debug) {
            fwrite(STDOUT, "\nIndex News\n");
        }

        $this->last_error = '';

        $arBulkData = array();

        \CModule::IncludeModule('iblock');

        $arFilter = array(
            'IBLOCK_ID' => $this->iblock_id,
        );

        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, array('ID', 'NAME', 'ACTIVE'));
        while ($arFields = $res->GetNext()) {
            if ($arFields['ACTIVE'] == 'Y') {
                $arBulkData[] = array(
                    'index' => array(
                        '_index' => ELASTICSEARCH_INDEX,
                        '_type'  => ElasticSearchClient::TYPE_NEWS,
                        '_id'    => $arFields['ID']
                    )
                );
                $arBulkData[] = array(
                    'title' => $arFields['~NAME']
                );
            } elseif ($this->es_client->docExist(ElasticSearchClient::TYPE_NEWS, $arFields['ID'])) {
                $arBulkData[] = array(
                    'delete' => array(
                        '_index' => ELASTICSEARCH_INDEX,
                        '_type'  => ElasticSearchClient::TYPE_NEWS,
                        '_id'    => $arFields['ID']
                    )
                );
            }
        }

        $bResult = true;

        if (count($arBulkData) > 0) {
            try {
                $response = $this->es_client->client->bulk(array('body' => $arBulkData));

                if ($response['errors']) {
                    $this->last_error = print_r($response, true);
                    $bResult = false;
                }

                unset($response);
            } catch(\Exceptions $ex) {
                $this->last_error = $ex->GetMessage();
                $bResult = false;
            }
        }

        unset($arBulkData);

        return $bResult;
    }

    /**
     * @param  int $iblock_id
     * @param  int $id
     * @return array
     */
    private static function getNewsById($iblock_id, $id)
    {
        $arResult = false;

        \CModule::IncludeModule('iblock');

        $arFilter = array(
            'IBLOCK_ID' => $iblock_id,
            'ID'        => $id
        );

        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, array('ID', 'NAME', 'ACTIVE'));
        while ($arFields = $res->GetNext()) {
            $arResult = $arFields;
        }

        return $arResult;
    }

    /**
     * @param  array &$arFields
     */
    public static function onAfterIBlockElementAdd(&$arFields)
    {
        $iblock_id = \CBInit::getInstance()->getIBlockID(self::IBLOCK_TYPE, self::IBLOCK_CODE);

        if (!isset($arFields['IBLOCK_ID']) || $arFields['IBLOCK_ID'] != $iblock_id || !isset($arFields['ID']) || intval($arFields['ID']) <= 0) {
            return;
        }

        $news = self::getNewsById($iblock_id, $arFields['ID']);

        if (($news === false) || ($news['ACTIVE'] == 'N')) {
            return;
        }

        $es = ElasticSearchClient::getInstance();
        $es->indexDoc( ElasticSearchClient::TYPE_NEWS, $arFields['ID'], array(
            'title' => $news['~NAME']
        ));
    }

    /**
     * @param  array &$arFields
     */
    public static function onAfterIBlockElementUpdate(&$arFields)
    {
        $iblock_id = \CBInit::getInstance()->getIBlockID(self::IBLOCK_TYPE, self::IBLOCK_CODE);

        if (!isset($arFields['IBLOCK_ID']) || $arFields['IBLOCK_ID'] != $iblock_id || !isset($arFields['ID']) || intval($arFields['ID']) <= 0) {
            return;
        }

        $news = self::getNewsById($iblock_id, $arFields['ID']);

        if ($news === false) {
            return;
        }

        $es = ElasticSearchClient::getInstance();

        if ($news['ACTIVE'] == 'Y') {
            $es->indexDoc(ElasticSearchClient::TYPE_NEWS, $arFields['ID'], array(
                'title' => $news['~NAME']
            ));
        } elseif ($es->docExist(ElasticSearchClient::TYPE_NEWS, $arFields['ID'])) {
            $es->deleteDoc(ElasticSearchClient::TYPE_NEWS, $arFields['ID']);
        }
    }

    /**
     * @param  array $arFields
     */
    public static function onAfterIBlockElementDelete($arFields)
    {
        $iblock_id = \CBInit::getInstance()->getIBlockID(self::IBLOCK_TYPE, self::IBLOCK_CODE);

        if (!isset($arFields['IBLOCK_ID']) || $arFields['IBLOCK_ID'] != $iblock_id || !isset($arFields['ID']) || intval($arFields['ID']) <= 0) {
            return;
        }

        $es = ElasticSearchClient::getInstance();

        if ($es->docExist(ElasticSearchClient::TYPE_NEWS, $arFields['ID'])) {
            $es->deleteDoc(ElasticSearchClient::TYPE_NEWS, $arFields['ID']);
        }
    }
}
