<?php
namespace ABCShop;

/**
 * Сравнить товары
 *
 */
class CompareProduct
{
    const STORE_KEY = 'COMPARE_PRODUCT';

    /**
     * @var CompareProduct
     */
    private static $instance;

    /**
     * @var array
     */
    private $arProduct;

    /**
     * @var int
     */
    private $count;

    /**
     * @var array
     */
    private $arType;

    /**
     * @var array
     */
    private $arProperty;

    /**
     * @var array
     */
    private $arData;

    private function __construct()
    {
        $this->arData = array();
        $this->arType = array();
        $this->arProperty = array();

        $this->restore();
    }

    private function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    private function __wakeup()
    {

    }

    /**
     * @return CompareProduct
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class();
        }

        return self::$instance;
    }

    /**
     * Запомнить данные
     */
    private function remember()
    {
        $GLOBALS['_SESSION'][self::STORE_KEY] = $this->arProduct;
    }

    /**
     * Восстановить данные
     */
    private function restore()
    {
        if (isset($GLOBALS['_SESSION'][self::STORE_KEY])) {
            $this->arProduct = $GLOBALS['_SESSION'][self::STORE_KEY];
        } else {
            $this->arProduct = array();
        }

        $this->count = count($this->arProduct);
    }

    /**
     * Кол-во товаров в списке
     * @return int
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * Добавить товар в список
     * @param string $product_id
     */
    public function add($product_id)
    {
        if ($this->exist($product_id)) {
            return;
        }

        $this->arProduct[$product_id] = $product_id;

        $this->count++;

        $this->remember();
    }

    /**
     * Удалить товар и списка
     * @param  string $product_id
     */
    public function remove($product_id)
    {
        if (!$this->exist($product_id)) {
            return;
        }

        unset($this->arProduct[$product_id]);

        $this->count--;

        $this->remember();
    }

    /**
     * Товар уже есть в списке
     * @param  string $product_id
     * @return boolean
     */
    public function exist($product_id)
    {
        return isset($this->arProduct[$product_id]);
    }

    public function compare()
    {
        $this->arData = array();
        $this->arType = array();
        $this->arProperty = array();

        $productMapper = Mapper::getInstance()->get('Catalog\Product', $GLOBALS['DB']);

        foreach ($this->arProduct as $product_id) {
            $product = $productMapper->findById($product_id);

            $type_id = $product->getType()->getId();

            if (!isset($this->arType[$type_id])) {
                $this->arType[$type_id] = array(
                    'name'  => $product->getType()->getName(),
                    'count' => 0
                );

                $this->arData[$type_id] = array();

                $this->arProperty[$type_id] = array();
            }

            $this->arType[$type_id]['count']++;

            $this->arData[$type_id][$product_id] = $product;
            $this->addPrice($product);

            $arProductProperty = $product->getProperty();
            foreach ($arProductProperty as $property_id => $property) {
                if (!isset($this->arProperty[$type_id][$property_id])) {
                    $this->arProperty[$type_id][$property_id] = array(
                        'name'     => $property->getName(),
                        'is_price' => false,
                        'diff'     => false,
                        'product'  => array()
                    );
                }

                $value = $property->getValue();
                $text = $value;

                $property_type = $property->getType();
                if ($property_type == Reference\PropertyType::ENUM) {
                    $text = $property->getOptions($value)->getValue();
                } elseif ($property_type == Reference\PropertyType::DECIMAL) {
                    $text = my_number_format($text, true);
                }

                $this->arProperty[$type_id][$property_id]['product'][$product_id] = array(
                    'value' => $value,
                    'text'  => $text,
                );
            }
        }

        foreach ($this->arProperty as $type_id => &$data) {
            uasort($data, array('\ABCShop\CompareProduct', 'sortProperty'));

            foreach ($data as &$item) {
                $first = false;
                foreach ($this->arData[$type_id] as $product_id => &$item_product) {
                    if (!isset($item['product'][$product_id])) {
                        $item['diff'] = true;
                        break;
                    } else {
                        $value = $item['product'][$product_id]['value'];
                    }

                    if ($first === false) {
                        $first = $value;
                    } elseif ($first != $value) {
                        $item['diff'] = true;
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param  array $a
     * @param  array $b
     * @return int
     */
    public static function sortProperty($a, $b)
    {
        //Поднимаем свойство с ценой наверх
        if ($a['is_price'] || $b['is_price']) {
            if ($a['is_price'] && $b['is_price']) {
                return 0;
            } elseif ($a['is_price']) {
                return -1;
            } elseif ($b['is_price']) {
                return 1;
            }
        }

        return strcasecmp($a['name'], $b['name']);
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return $this->arType;
    }

    /**
     * @param  int $type_id
     * @return array
     */
    public function getProductList($type_id)
    {
        if (!isset($this->arData[$type_id])) {
            return array();
        }

        return $this->arData[$type_id];
    }

    /**
     * @param  int $type_id
     * @return array
     */
    public function getPropertyList($type_id)
    {
        if (!isset($this->arProperty[$type_id])) {
            return array();
        }

        return $this->arProperty[$type_id];
    }

    /**
     * Добавить цену
     * @param \ABCShop\Catalog\Product $product
     */
    private function addPrice($product)
    {
        $type_id = $product->getType()->getId();

        if (!isset($this->arProperty[$type_id]['price'])) {
            $this->arProperty[$type_id]['price'] = array(
                'name'     => 'Цена',
                'is_price' => true,
                'diff'     => false,
                'product'  => array()
            );
        }

        $product_id = $product->getId();
        $price = $product->getPrice(getProductPriceType());

        if ($price === false || $price == 0) {
            $text = 'цена по запросу';
        } else {
            $text = my_number_format($price).' р.';
        }

        $this->arProperty[$type_id]['price']['product'][$product_id] = array(
            'value' => $price,
            'text'  => $text,
        );
    }
}
